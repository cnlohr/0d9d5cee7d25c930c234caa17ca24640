```c
	void Setup()
	{
		RCC->APB1ENR |= RCC_APB1ENR_TIM14EN;
		TIM14->PSC = 48-1; //Counts up in microseconds
		TIM14->ARR = 65535;
		TIM14->CNT = 0;
		TIM14->CR1 = 1;
		TIM14->DIER = TIM_DIER_CC2IE | TIM_DIER_UIE;
    
    
		NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 0x03; //High priority.
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}

	void TIM14_IRQHandler(void)
	{
		if( TIM14->SR & TIM_SR_CC1IF )
		{
			TIM14->SR &= ~TIM_SR_CC1IF;
			tim14counterA++; //Compare match
		}
		if( TIM14->SR & TIM_SR_UIF )
		{
			TIM14->SR &= ~TIM_SR_UIF;
			tim14counterB++;	//Overflow match
		}
	}
```